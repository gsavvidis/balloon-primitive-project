//Include libraries
#include <RF22.h>
#include <RF22Router.h>

//Define constants
#define SOURCE_ADDRESS1 13
#define SOURCE_ADDRESS2 21
#define SOURCE_ADDRESS3 15
#define DESTINATION_ADDRESS 3

//init rf22
RF22Router rf22(DESTINATION_ADDRESS);

//global parameters
boolean successful_message=false; //message success
int minMotorPeriod=500; //minimum possible stepper motor period
int maxMotorPeriod=2000; //maximum possible stepper motor period
// power parameters
uint8_t rssi; //non-processed power from transmitter
float PrLocal=0; //processed power from transmitter
float Pr[3]; //maximum power values for each transmitter node
float maxPr=-40; //maximum transmitter power
float minPr=-75; //minimum transmitter power
float desPr=-60; //desired transmitter power, which translates to the desired balloon position
//diagnostics
//int failed_transmissions = 0;
//int correct_transmissions = 0;
int numberOfBytes=0;
int totalNumberOfBytes=0;
float successful_perc;
float throughput;
//timekeeping
int start_time; //start time of diagnostics
int diagnostics_time=600000; //timestep of diagnostics
int current_time;
int delta_time = 0; //time for sendtoWait
int initial_time; //time before sendtoWait
int start_time_position; //start time of node power calculation
int power_time=5000; //timestep of node power calculation


void setup() {
    Serial.begin(9600);
  if (!rf22.init())
    Serial.println("RF22 init failed");
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
  if (!rf22.setFrequency(433.5))
    Serial.println("setFrequency Fail");
  rf22.setTxPower(RF22_TXPOW_20DBM);
  //1,2,5,8,11,14,17,20 DBM
  rf22.setModemConfig(RF22::GFSK_Rb125Fd125);

  // Manually define the routes for this network
  rf22.addRouteTo(SOURCE_ADDRESS1, SOURCE_ADDRESS1);
  rf22.addRouteTo(SOURCE_ADDRESS2, SOURCE_ADDRESS2);
  rf22.addRouteTo(SOURCE_ADDRESS3, SOURCE_ADDRESS3);
  // init random number generator
  Pr[0]=-1000;
  Pr[1]=-1000;
  Pr[2]=-1000;
  randomSeed(1);
  start_time=millis();
  start_time_position=millis();
}

void loop() {
  // initialize all required parameters for data receive
  uint8_t buf[RF22_ROUTER_MAX_MESSAGE_LEN];
  char incoming[RF22_ROUTER_MAX_MESSAGE_LEN];
  memset(buf, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
  memset(incoming, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
  uint8_t len = sizeof(buf);
  uint8_t from;
  int received_value = 0;
  initial_time = millis();
  //receive measurement data
  if (rf22.recvfromAck(buf, &len, &from))
  {
    buf[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
    memcpy(incoming, buf, RF22_ROUTER_MAX_MESSAGE_LEN);
    Serial.print("got request from : ");
    Serial.println(from, DEC);
    received_value = atoi((char*)incoming);

    // if the data transmission is successful, update diagnostics
    numberOfBytes=sizeof(incoming);
    totalNumberOfBytes+=numberOfBytes;
    current_time = millis();
    delta_time+= current_time - initial_time;

    /* if the transmitter is node 1, which in our experimental setup is the node that has the sensor & the stepper motor
    // then split all the measurement data
    // otherwise get the sole transmitted value, from nodes 2&3
    // In the realistic scenario this if wouldn't be usefull since all nodes would be like node 1*/
    if(from==SOURCE_ADDRESS1){
      const char s[2] = " ";
      char *token;
      token = strtok(incoming,s);
      int temperature = atoi(token);
      float temperature2=(float)temperature/100;
      token = strtok(NULL,s);
      int pressure = atoi(token);
      float pressure2=(float)pressure/100;
      token = strtok(NULL,s);
      int altitude = atoi(token);
      float altitude2=(float)altitude/100;
      Serial.print("Balona [");
      Serial.print(from);
      Serial.print("]: Temperature: ");
      Serial.print(temperature2);
      Serial.print(", Pressure: ");
      Serial.print(pressure2);
      Serial.print(", Altitude: ");
      Serial.println(altitude2);
      temperature=0;
      pressure=0;
      altitude=0;
    }
    else{
      float received_value2=(float)received_value/100;
      Serial.print("Balona [");
      Serial.print(from);
      Serial.print("]: ");
      Serial.println(received_value2);
      received_value=0;
    }
    Serial.println();
    // Get the transmitter power, this way we can estimate the transmitter node distance from the receiver.
    /* In our implementation a GPS should be used for more precise position estimation, since the distance does not show
    // the coordinates in each reference frame axis. But for presentation purposes this approach is deemed sufficient.*/
    rssi=rf22.rssiRead();
    PrLocal=((float)rssi-230.0)/1.8;
    // Identify the transmitter node and compare the value to the maximum one.
    if (from == SOURCE_ADDRESS1){
      if(PrLocal>Pr[0]){
        Pr[0]=PrLocal;
      }
    }
    else if (from == SOURCE_ADDRESS2){
      if(PrLocal>Pr[1]){
        Pr[1]=PrLocal;
      }
    }
    else if (from == SOURCE_ADDRESS3){
      if(PrLocal>Pr[2]){
        Pr[2]=PrLocal;
      }
    }
    PrLocal=0;
    
    // initialize all required parameters for success message transmission
    char data_read[RF22_ROUTER_MAX_MESSAGE_LEN];
    uint8_t data_send[RF22_ROUTER_MAX_MESSAGE_LEN];
    memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    sprintf(data_read, "%d", from);
    data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
    memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);
    successful_message = false;
    // send success message to the correct node, the success message is this node's number
    // this way the transmitter node can understand if the receiver thought the transmitted data was from the correct node.
    if (rf22.sendtoWait(data_send, sizeof(data_send), from) != RF22_ROUTER_ERROR_NONE)
    {
      Serial.println("sendtoWait failed");
    }
    else{
      successful_message=true;
      // if the transmitter node is Node 1, we  send the control messages to its actuators
      /* In the realistic scenario, we would be sending this data to all the nodes, since all the nodes have actuators
       *  but for the presentation purposes 1 Node is deemed sufficient
       */
      if (from==SOURCE_ADDRESS1){
        /*
         * In the more realistic scenario, in this point the received measurement data would be processed
         * and the desired balloon attitude would be estimated. This way we would be sending the desired
         * valve opening and the two desired rotational periods of the stepper motors 
         * for a specified number of total microsteps.
         * In a more intermediate implementation we could also define the rotation microsteps, but we think that
         * defining just the rotational period would be enough to fully control the balloon.
         */
         /*
          * In this simple implementation, we donot have any information for the [X/Y] plane
          * hence we cannot define a desired period for the stepper motor.
          * But, for presentation purposes we calculate the transmitter power, hence we can translate this
          * calculation to position in one axis and then to the desired stepper motor period.
          */
        // This translates power to the desired rotational period of the stepper motor with a range of [500,2000]
        // Analytically this division calculates the percentage of the existing rotational period we can give.
        float control_variable_perc=(Pr[0]-desPr)/max(abs(desPr-maxPr),abs(desPr-(-minPr)));
        int control_variable=1000;
        // then we calculate the percentage with regards to the 2000 minimum motor period 
        /* at this point it is deemed necessary explain that regularly when control_variable_perc we could just turn
        * the actuator off, since we don't actually need more force, but since this has to be tested and verified 
        * in a more realistic environment we discarded this option*/
        if(control_variable_perc>=0){
          control_variable=(int)maxMotorPeriod*(1-control_variable_perc); 
        }
        else if(control_variable_perc<0){
          control_variable=(int)-maxMotorPeriod*(1+control_variable_perc); 
        }
        /*
         * As with most motors, they donot operate under/over a certain number of rotational speed, hence
         * if this number is calculated lower we simply set it to the minimum period (maximum rotational speed)
         */
        if(abs(control_variable)<minMotorPeriod && control_variable>=0){
          control_variable=minMotorPeriod;
        }
        else if(abs(control_variable)<minMotorPeriod && control_variable<0){
          control_variable=-minMotorPeriod;
        }
        //[OUTDATED IMPLEMENTATION] BASED ON RANDOM CONTROL VARIABLE
        /*int control_variable=random(500,2000);
        // random sign number, if the control variable is positive then we rotate clockwise
        // otherwise counterclockwise
        int sign=random(0,1);
        if(sign==0){
          control_variable*=-1;
        }*/
        // initialize all required parameters for success message transmission
        char data_read[RF22_ROUTER_MAX_MESSAGE_LEN];
        uint8_t data_send[RF22_ROUTER_MAX_MESSAGE_LEN];
        memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
        memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
        sprintf(data_read, "%d", control_variable);
        data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
        memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);  
        // send control data to Node 1
        if (rf22.sendtoWait(data_send, sizeof(data_send), from) != RF22_ROUTER_ERROR_NONE){
          Serial.println("sendtoWait failed");
        }
      }
      
    }
  }
// when the current time is more than the diagnostics period, print and reset all diagnostics
if((current_time - start_time)>=diagnostics_time){

  throughput= 8*(float)(totalNumberOfBytes)*1000/delta_time;
  delta_time=0;
  totalNumberOfBytes=0;
  Serial.println("=== Diagnostics ===");
  Serial.print("Throughput= ");
  Serial.print(throughput);
  Serial.println("bps");

  start_time=millis();
  Serial.println();
}
// when the current time is more than the power period, print and reset all power/position measurements
if((current_time - start_time_position)>=power_time){
  Serial.println("=== Power ===");
  Serial.println("Power1= ");
  Serial.println(Pr[0]);
  Serial.println("Power2= ");
  Serial.println(Pr[1]);
  Serial.println("Power3= ");
  Serial.println(Pr[2]);
  Pr[0]=-1000;
  Pr[1]=-1000;
  Pr[2]=-1000;
  start_time_position=millis();
  Serial.println();
}
successful_message=false;
}
