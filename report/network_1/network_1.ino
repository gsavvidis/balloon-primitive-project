
void loop() {
  sprintf(data_read, "%d %d %d", sensorVal2[0], sensorVal2[1], sensorVal2[2]);
  data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
  memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);
  initial_time = millis();
  //send required data
  rf22.sendtoWait(data_send, sizeof(data_send), DESTINATION_ADDRESS) != RF22_ROUTER_ERROR_NONE
  //receive success message, wait for a maximum of 3 seconds
  if(rf22.recvfromAckTimeout(buf, &len, 3000, &from)){
    /* The transmitted data are being processed by the receiver. The receiver then sends
    // control data to this node. These data are how much to open the valve of the balloon
    // and at what speed(frequency/period) to turn the stepper motors in order to produce 
    // the desired force to the balloon and move it.
    // To simplify this implementation we are not using a valve and only one stepper motor
    // is used, hence we receive one value which is the period of the stepper motor rotation
    // which sequentually controls the balloon.*/
    // initialize all required parameters for data receive
    uint8_t buf[RF22_ROUTER_MAX_MESSAGE_LEN];
    char incoming[RF22_ROUTER_MAX_MESSAGE_LEN];
    memset(buf, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    memset(incoming, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    uint8_t len = sizeof(buf);
    uint8_t from;
    //receive data, wait for a maximum of 5 seconds
    if(rf22.recvfromAckTimeout(buf, &len, 5000, &from)){
      buf[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
      memcpy(incoming, buf, RF22_ROUTER_MAX_MESSAGE_LEN);
      control_variable = atoi((char*)incoming);
      Serial.print("Control variable: ");
      Serial.println(control_variable);
    }
  }
}
