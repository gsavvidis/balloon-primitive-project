void loop() {
  // initialize all required parameters for data transmission
  char data_read[RF22_ROUTER_MAX_MESSAGE_LEN];
  uint8_t data_send[RF22_ROUTER_MAX_MESSAGE_LEN];
  while (!successful_packet)
  {
    memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    sprintf(data_read, "%d", sensorVal2[0]);
    data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
    memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);
    initial_time = millis();
    //send required data
    if (rf22.sendtoWait(data_send, sizeof(data_send), DESTINATION_ADDRESS) != RF22_ROUTER_ERROR_NONE)
    {
      // if the data transmission is unsuccessful, delay for a number of ms and update diagnostics
      Serial.println("sendtoWait failed");
      randNumber=random(200,max_delay);
      Serial.println(randNumber);
      delay(randNumber);
      failed_transmissions++;
    }
    else
    {
      // if the data transmission is successful, update diagnostics
      //successful_counter++; /*this is if we were to send the measurements one by one*/
      successful_packet=true;
      correct_transmissions++;
      numberOfBytes=sizeof(data_send);
      totalNumberOfBytes+=numberOfBytes;
      current_time = millis();
      delta_time+= current_time - initial_time;
      Serial.println("sendtoWait Succesful");
    }
  }
}
