//diagnostics
//int failed_transmissions = 0;
//int correct_transmissions = 0;
int numberOfBytes=0;
int totalNumberOfBytes=0;
float successful_perc;
float throughput;
//timekeeping
int start_time; //start time of diagnostics
int diagnostics_time=600000; //timestep of diagnostics
int current_time;
int delta_time = 0; //time for sendtoWait
int initial_time; //time before sendtoWait

void setup() {
  start_time=millis();
}

void loop() {
  initial_time = millis();
  rf22.recvfromAck(buf, &len, &from)
  // if the data transmission is successful, update diagnostics
  numberOfBytes=sizeof(incoming);
  totalNumberOfBytes+=numberOfBytes;
  current_time = millis();
  delta_time+= current_time - initial_time;
// when the current time is more than the diagnostics period, print and reset all diagnostics
if((current_time - start_time)>=diagnostics_time){
  throughput= 8*(float)(totalNumberOfBytes)*1000/delta_time;
  delta_time=0;
  totalNumberOfBytes=0;
  Serial.println("=== Diagnostics ===");
  Serial.print("Throughput= ");
  Serial.print(throughput);
  Serial.println("bps");

  start_time=millis();
  Serial.println();
}
}
