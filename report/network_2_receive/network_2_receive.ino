
void loop() {
  rf22.recvfromAck(buf, &len, &from)
  /* if the transmitter is node 1, which in our experimental setup is the node that has the sensor & the stepper motor
  // then split all the measurement data
  // otherwise get the sole transmitted value, from nodes 2&3
  // In the realistic scenario this if wouldn't be usefull since all nodes would be like node 1*/
  if(from==SOURCE_ADDRESS1){
    const char s[2] = " ";
    char *token;
    token = strtok(incoming,s);
    int temperature = atoi(token);
    float temperature2=(float)temperature/100;
    token = strtok(NULL,s);
    int pressure = atoi(token);
    float pressure2=(float)pressure/100;
    token = strtok(NULL,s);
    int altitude = atoi(token);
    float altitude2=(float)altitude/100;
    Serial.print("Balona [");
    Serial.print(from);
    Serial.print("]: Temperature: ");
    Serial.print(temperature2);
    Serial.print(", Pressure: ");
    Serial.print(pressure2);
    Serial.print(", Altitude: ");
    Serial.println(altitude2);
    temperature=0;
    pressure=0;
    altitude=0;
  }
  else{
    float received_value2=(float)received_value/100;
    Serial.print("Balona [");
    Serial.print(from);
    Serial.print("]: ");
    Serial.println(received_value2);
    received_value=0;
  }
  Serial.println();
}
