void loop() {
  rf22.sendtoWait(data_send, sizeof(data_send), DESTINATION_ADDRESS) != RF22_ROUTER_ERROR_NONE
  // the transmitter will now receive a success message from the receiver node, so as to be aware
  // of the successful data transmission from this node
  // initialize all required parameters for data receive
  uint8_t buf[RF22_ROUTER_MAX_MESSAGE_LEN];
  char incoming[RF22_ROUTER_MAX_MESSAGE_LEN];
  memset(buf, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
  memset(incoming, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
  uint8_t len = sizeof(buf);
  uint8_t from;
  int received_value = 0;
  //receive success message, wait for a maximum of 3 seconds
  if(rf22.recvfromAckTimeout(buf, &len, 3000, &from)){
    buf[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
    memcpy(incoming, buf, RF22_ROUTER_MAX_MESSAGE_LEN);
    received_value = atoi((char*)incoming);
    // if the received message is this node's address, then the correct node was received
    // otherwise the receiver thought it received from another node than the one we sent
    if (received_value == SOURCE_ADDRESS){
      Serial.print("Confirmation Message: ");
      Serial.println(received_value);
      successful_packet = true;
    }
    else{
      Serial.println("Wrong Transmitter Data");
    }
  }
}
