// power parameters
uint8_t rssi; //non-processed power from transmitter
float PrLocal=0; //processed power from transmitter
float Pr[3]; //maximum power values for each transmitter node
//timekeeping
int start_time_position; //start time of node power calculation
int power_time=5000; //timestep of node power calculation

void setup() {
  start_time_position=millis();
}

void loop() {
  rf22.recvfromAck(buf, &len, &from)
  //if the data transmission is correct
  current_time = millis();
  // Get the transmitter power, this way we can estimate the transmitter node distance from the receiver.
  /* In our implementation a GPS should be used for more precise position estimation, since the distance does not show
  // the coordinates in each reference frame axis. But for presentation purposes this approach is deemed sufficient.*/
  rssi=rf22.rssiRead();
  PrLocal=((float)rssi-230.0)/1.8;
  // Identify the transmitter node and compare the value to the maximum one.
  if (from == SOURCE_ADDRESS1){
    if(PrLocal>Pr[0]){
      Pr[0]=PrLocal;
    }
  }
  else if (from == SOURCE_ADDRESS2){
    if(PrLocal>Pr[1]){
      Pr[1]=PrLocal;
    }
  }
  else if (from == SOURCE_ADDRESS3){
    if(PrLocal>Pr[2]){
      Pr[2]=PrLocal;
    }
  }
  PrLocal=0;
// when the current time is more than the power period, print and reset all power/position measurements
if((current_time - start_time_position)>=power_time){
  Serial.println("=== Power ===");
  Serial.println("Power1= ");
  Serial.println(Pr[0]);
  Serial.println("Power2= ");
  Serial.println(Pr[1]);
  Serial.println("Power3= ");
  Serial.println(Pr[2]);
  Pr[0]=-1000;
  Pr[1]=-1000;
  Pr[2]=-1000;
  start_time_position=millis();
  Serial.println();
}
}
