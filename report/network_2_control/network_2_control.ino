
void loop() {
  // put your main code here, to run repeatedly:
  if (rf22.recvfromAck(buf, &len, &from))
  {
    if (rf22.sendtoWait(data_send, sizeof(data_send), from) != RF22_ROUTER_ERROR_NONE)
    {
      Serial.println("sendtoWait failed");
    }
    else{
      successful_message=true;
      // if the transmitter node is Node 1, we  send the control messages to its actuators
      /* In the realistic scenario, we would be sending this data to all the nodes, since all the nodes have actuators
       *  but for the presentation purposes 1 Node is deemed sufficient
       */
      if (from==SOURCE_ADDRESS1){
        /*
         * Here we calculate the control_variable. This will be analytically explained later on the report.
         */
          // initialize all required parameters for success message transmission
        char data_read[RF22_ROUTER_MAX_MESSAGE_LEN];
        uint8_t data_send[RF22_ROUTER_MAX_MESSAGE_LEN];
        memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
        memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
        sprintf(data_read, "%d", control_variable);
        data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
        memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);  
        // send control data to Node 1
        if (rf22.sendtoWait(data_send, sizeof(data_send), from) != RF22_ROUTER_ERROR_NONE){
          Serial.println("sendtoWait failed");
        }
      }
    }
  }
}
