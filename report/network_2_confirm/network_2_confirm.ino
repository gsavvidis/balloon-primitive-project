void loop() {
  rf22.recvfromAck(buf, &len, &from)
  // initialize all required parameters for success message transmission
  char data_read[RF22_ROUTER_MAX_MESSAGE_LEN];
  uint8_t data_send[RF22_ROUTER_MAX_MESSAGE_LEN];
  memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
  memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
  sprintf(data_read, "%d", from);
  data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
  memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);
  successful_message = false;
  // send success message to the correct node, the success message is this node's number
  // this way the transmitter node can understand if the receiver thought the transmitted data was from the correct node.
  if (rf22.sendtoWait(data_send, sizeof(data_send), from) != RF22_ROUTER_ERROR_NONE)
  {
    Serial.println("sendtoWait failed");
  }
  else{
    successful_message=true;
  }
}
