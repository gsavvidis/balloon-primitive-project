//Include libraries
#include <RF22.h>
#include <RF22Router.h>

//Define constants
#define SOURCE_ADDRESS 13 //Number of Tx
#define DESTINATION_ADDRESS 3 //Number of Rx to send

//init rf22
RF22Router rf22(SOURCE_ADDRESS);

void setup() {
    Serial.begin(9600);
  if (!rf22.init())
    Serial.println("RF22 init failed");
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
  if (!rf22.setFrequency(433.5))
    Serial.println("setFrequency Fail");
  rf22.setTxPower(RF22_TXPOW_20DBM);
  //1,2,5,8,11,14,17,20 DBM
  rf22.setModemConfig(RF22::GFSK_Rb125Fd125);
  // Manually define the routes for this network
  rf22.addRouteTo(DESTINATION_ADDRESS, DESTINATION_ADDRESS);

}
