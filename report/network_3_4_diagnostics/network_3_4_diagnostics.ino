//diagnostics
int failed_transmissions = 0;
int correct_transmissions = 0;
int numberOfBytes=0;
int totalNumberOfBytes=0;
float successful_perc;
float throughput;
//timekeeping
int start_time; //start time of diagnostics
int measurement_time=10000; //timestep of diagnostics
int current_time;
int delta_time = 0; //time for sendtoWait
int initial_time; //time before sendtoWait

void setup() {
  start_time=millis();
}

void loop() {
  initial_time = millis();
  // if the data transmission is successful, update diagnostics
  correct_transmissions++;
  numberOfBytes=sizeof(data_send);
  totalNumberOfBytes+=numberOfBytes;
  current_time = millis();
  delta_time+= current_time - initial_time;
  Serial.println("sendtoWait Succesful");
  successful_packet = true;
  current_time = millis();
  
  // when the current time is more than the diagnostics period, print and reset all diagnostics
if((current_time - start_time)>=measurement_time){
  successful_perc=(float)correct_transmissions/(correct_transmissions+failed_transmissions);
  correct_transmissions=0;
  failed_transmissions=0;
  Serial.println();
  Serial.println("=== Diagnostics ===");
  Serial.print("Successful Ratio= ");
  Serial.println(successful_perc);

  throughput= 8*(float)(totalNumberOfBytes)*1000/delta_time;
  delta_time=0;
  totalNumberOfBytes=0;
  Serial.print("Throughput= ");
  Serial.print(throughput);
  Serial.println("bps");

  start_time=millis();
}
}
