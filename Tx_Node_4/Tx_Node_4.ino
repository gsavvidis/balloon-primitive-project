//Include libraries
#include <RF22.h>
#include <RF22Router.h>

//Define constants
#define SOURCE_ADDRESS 15 //Number of Tx
#define DESTINATION_ADDRESS 3 //Number of Rx to send

//init rf22
RF22Router rf22(SOURCE_ADDRESS);

//global parameters
float sensorVal = 69; //sensor measurement
boolean successful_packet = false; //packet success
long randNumber; //random number for the next sendToWait
int max_delay=3000; //maximum random number for the next sendtoWait
//diagnostics
int failed_transmissions = 0;
int correct_transmissions = 0;
int numberOfBytes=0;
int totalNumberOfBytes=0;
float successful_perc;
float throughput;
//timekeeping
int start_time; //start time of diagnostics
int measurement_time=20000; //timestep of diagnostics
int current_time;
int delta_time = 0; //time for sendtoWait
int initial_time; //time before sendtoWait


void setup() {
  Serial.begin(9600);
  if (!rf22.init())
    Serial.println("RF22 init failed");
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
  if (!rf22.setFrequency(433.5))
    Serial.println("setFrequency Fail");
  rf22.setTxPower(RF22_TXPOW_20DBM);
  //1,2,5,8,11,14,17,20 DBM
  rf22.setModemConfig(RF22::GFSK_Rb125Fd125);
  // Manually define the routes for this network
  rf22.addRouteTo(DESTINATION_ADDRESS, DESTINATION_ADDRESS);
  // init random number generator
  sensorVal = analogRead(A0);
  randomSeed(sensorVal);
  start_time=millis();
}

void loop() {
  randNumber=random(200,250);
  delay(randNumber);
  // Get Measurement
  //sensorVal = analogRead(A0);
  sensorVal=79.79;
  Serial.print("Sensor Value: ");
  Serial.println(sensorVal);

  //typecast the sent data to integer with 3 decimal point accuracy
  sensorVal*=100;
  int sensorVal2=(int)sensorVal;

  // initialize all required parameters for data transmission
  char data_read[RF22_ROUTER_MAX_MESSAGE_LEN];
  uint8_t data_send[RF22_ROUTER_MAX_MESSAGE_LEN];
  memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
  memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
  sprintf(data_read, "%d", sensorVal2);
  data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
  memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);
  successful_packet = false;
  initial_time = millis();
  //run this loop until we have a successfull packet
  while (!successful_packet)
  {
    //send required data
    if (rf22.sendtoWait(data_send, sizeof(data_send), DESTINATION_ADDRESS) != RF22_ROUTER_ERROR_NONE)
    {
      // if the data transmission is unsuccessful, delay for a number of ms and update diagnostics
      Serial.println("sendtoWait failed");
      randNumber=random(200,max_delay);
      Serial.println(randNumber);
      delay(randNumber);
      failed_transmissions++;
    }
    else
    {
      
      // if the data transmission is successful, update diagnostics
      correct_transmissions++;
      numberOfBytes=sizeof(data_send);
      totalNumberOfBytes+=numberOfBytes;
      current_time = millis();
      delta_time+= current_time - initial_time;
      Serial.println("sendtoWait Succesful");

      // the transmitter will now receive a success message from the receiver node, so as to be aware
      // of the successful data transmission from this node
      // initialize all required parameters for data receive
      uint8_t buf[RF22_ROUTER_MAX_MESSAGE_LEN];
      char incoming[RF22_ROUTER_MAX_MESSAGE_LEN];
      memset(buf, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      memset(incoming, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      uint8_t len = sizeof(buf);
      uint8_t from;
      int received_value = 0;
      //receive success message, wait for a maximum of 3 seconds
      if(rf22.recvfromAckTimeout(buf, &len, 3000, &from)){
      buf[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
      memcpy(incoming, buf, RF22_ROUTER_MAX_MESSAGE_LEN);
      received_value = atoi((char*)incoming);
      // if the received message is this node's address, then the correct node was received
      // otherwise the receiver thought it received from another node than the one we sent
      if (received_value == SOURCE_ADDRESS){
      Serial.print("Confirmation Message: ");
      Serial.println(received_value);
      successful_packet = true;
      }
      else{
        Serial.println("Wrong Transmitter Data");
      }
      }
      current_time = millis();
    }

  }
// when the current time is more than the diagnostics period, print and reset all diagnostics
if((current_time - start_time)>=measurement_time){
  successful_perc=(float)correct_transmissions/(correct_transmissions+failed_transmissions);
  correct_transmissions=0;
  failed_transmissions=0;
  Serial.println(); 
  Serial.println("=== Diagnostics ==="); 
  Serial.println("Successful Ratio= ");
  Serial.println(successful_perc);

  throughput= 8*(float)(totalNumberOfBytes)*1000/delta_time;
  delta_time=0;
  totalNumberOfBytes=0;
  Serial.print("Throughput= "); 
  Serial.print(throughput); 
  Serial.println("bps");

  start_time=millis();
}
Serial.println();
}
