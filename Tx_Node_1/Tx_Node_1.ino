//Include libraries
#include <RF22.h>
#include <RF22Router.h>
#include <Wire.h> //I2C library
#include <Adafruit_BMP280.h> //sensor library
#include <SPI.h> //SPI library
//#include <AccelStepper.h> //stepper motor library
//Temp sensor libraries
#include <OneWire.h> 
#include <DallasTemperature.h>

//Define constants
#define SOURCE_ADDRESS 13 //Number of Tx
#define DESTINATION_ADDRESS 3 //Number of Rx to send
// SPI constants
#define BMP_SCK (13)
#define BMP_MISO (12)
#define BMP_MOSI (11)
#define BMP_CS (10)

#define dirPin 6 //stepper motor direction pin
#define stepPin 7 //stepper motor step pin
#define stepsPerRevolution 200 //stepper motor steps required for 1 revolution
#define ONE_WIRE_BUS 4

//init rf22 & sensor
RF22Router rf22(SOURCE_ADDRESS);
//Adafruit_BMP280 bmp; //I2C implementation
//Adafruit_BMP280 bmp(BMP_CS); //Hardware SPI implementation
Adafruit_BMP280 bmp(BMP_CS,BMP_MOSI,BMP_MISO,BMP_SCK); //Software SPI implementation
OneWire oneWire(ONE_WIRE_BUS); // temp sensor begin
DallasTemperature sensors(&oneWire); //dallas init

//global parameters
float sensorVal[3]; //sensor measurement
int control_variable=1000; //control variable for stepper motor
boolean successful_packet = false; //packet success
long randNumber; //random number for the next sendToWait
int max_delay=3000; //maximum random number for the next sendtoWait
//diagnostics
int failed_transmissions = 0;
int correct_transmissions = 0;
int numberOfBytes=0;
int totalNumberOfBytes=0;
float successful_perc;
float throughput;
//timekeeping
int start_time; //start time of diagnostics
int measurement_time=20000; //timestep of diagnostics
int current_time;
int delta_time = 0; //time for sendtoWait
int initial_time; //time before sendtoWait


void setup() {
  Serial.begin(9600);
  if (!rf22.init())
    Serial.println("RF22 init failed");
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
  if (!rf22.setFrequency(433.5))
    Serial.println("setFrequency Fail");
  rf22.setTxPower(RF22_TXPOW_20DBM);
  //1,2,5,8,11,14,17,20 DBM
  rf22.setModemConfig(RF22::GFSK_Rb125Fd125);
  // Manually define the routes for this network
  rf22.addRouteTo(DESTINATION_ADDRESS, DESTINATION_ADDRESS);

  //initialize sensor
  /*if (!bmp.begin()){
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }/*
  //init settings for sensor
    /* Default settings from datasheet. */
  //bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
  //                Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
  //               Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
   //               Adafruit_BMP280::FILTER_X16,      /* Filtering. */
   //               Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */

  //set pins for stepper motor
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);

  //start temp sensor
  sensors.begin(); 
  // init random number generator
  //sensorVal[0] = bmp.readTemperature();
  //sensorVal[1] = bmp.readPressure();
  //sensorVal[2] = bmp.readAltitude(1019);
  //randomSeed(sensorVal[0]);
  randomSeed(1);
  start_time=millis();
}

void loop() {
  randNumber=random(50,100);
  delay(randNumber);
  // Get Measurements
  /*sensorVal[0] = analogRead(A0);
  sensorVal[1] = analogRead(A1);
  sensorVal[2] = analogRead(A2);*/
  // Pressure/Temp/Altitude Sensor
  
  /*sensorVal[0] = bmp.readTemperature(); //Celsius
  sensorVal[1] = bmp.readPressure()/100; //hPa
  sensorVal[2] = bmp.readAltitude(1019); //metres*/
  
  sensors.requestTemperatures();
  sensorVal[0]=sensors.getTempCByIndex(0);
  //sensorVal[0]=69.69;
  sensorVal[1]=70.69;
  sensorVal[2]=71.69;

  Serial.print("Temperature: ");
  Serial.print(sensorVal[0]);
  Serial.println(" *C");
  Serial.print("Pressure: ");
  Serial.print(sensorVal[1]);
  Serial.println(" hPa");
  Serial.print("Altitude: ");
  Serial.print(sensorVal[2]);
  Serial.println(" m");

  //typecast the sent data to integer with 3 decimal point accuracy
  sensorVal[0]*=100;
  sensorVal[1]*=100;
  sensorVal[2]*=100;

  int sensorVal2[3];
  sensorVal2[0]=(int)sensorVal[0];
  sensorVal2[1]=(int)sensorVal[1];
  sensorVal2[2]=(int)sensorVal[2];

  //[OUTDATED IDEA]
  // int to string and a word in front saying what it is "Temp,Alt,Pres"
  // split strings based on character(s), atoi() the 2nd and identify the measured value
  
  // Stepper motor
  //max current = 2.5A for 25V
  //set the direction clockwise or counterclockwise
  if(control_variable>=0){
    digitalWrite(dirPin, HIGH);
  }
  else if(control_variable<0){
    digitalWrite(dirPin, LOW);
  }
  //turn the motor for a constantnumber of microsteps
  for (int i = 0; i < stepsPerRevolution; i++) {
    // These four lines result in 1 microstep:
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(abs(control_variable));
    digitalWrite(stepPin, LOW);
    delayMicroseconds(abs(control_variable));
  }
  
  // initialize all required parameters for data transmission
  char data_read[RF22_ROUTER_MAX_MESSAGE_LEN];
  uint8_t data_send[RF22_ROUTER_MAX_MESSAGE_LEN];
  /*int successful_counter=0;
  for(int i=0;i<3;i++){*/ /*this is if we were to send the measurements one by one*/
  successful_packet = false;
  //run this loop until we have a successfull packet
  while (!successful_packet)
  {
    memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    //sprintf(data_read, "%d", sensorVal2);
    //sprintf(data_read, "%d", sensorVal2[0]);
    sprintf(data_read, "%d %d %d", sensorVal2[0], sensorVal2[1], sensorVal2[2]);
    data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
    memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);
    initial_time = millis();
    //send required data
    if (rf22.sendtoWait(data_send, sizeof(data_send), DESTINATION_ADDRESS) != RF22_ROUTER_ERROR_NONE)
    {
      // if the data transmission is unsuccessful, delay for a number of ms and update diagnostics
      Serial.println("sendtoWait failed");
      randNumber=random(200,max_delay);
      Serial.println(randNumber);
      delay(randNumber);
      failed_transmissions++;
    }
    else
    {
      // if the data transmission is successful, update diagnostics
      //successful_counter++; /*this is if we were to send the measurements one by one*/
      correct_transmissions++;
      numberOfBytes=sizeof(data_send);
      totalNumberOfBytes+=numberOfBytes;
      current_time = millis();
      delta_time+= current_time - initial_time;
      Serial.println("sendtoWait Succesful");

      // the transmitter will now receive a success message from the receiver node, so as to be aware
      // of the successful data transmission from this node
      // initialize all required parameters for data receive
      uint8_t buf[RF22_ROUTER_MAX_MESSAGE_LEN];
      char incoming[RF22_ROUTER_MAX_MESSAGE_LEN];
      memset(buf, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      memset(incoming, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      uint8_t len = sizeof(buf);
      uint8_t from;
      int received_value = 0;
      //receive success message, wait for a maximum of 3 seconds
      if(rf22.recvfromAckTimeout(buf, &len, 3000, &from)){
        buf[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
        memcpy(incoming, buf, RF22_ROUTER_MAX_MESSAGE_LEN);
        received_value = atoi((char*)incoming);
        // if the received message is this node's address, then the correct node was received
        // otherwise the receiver thought it received from another node than the one we sent
        if (received_value == SOURCE_ADDRESS){
          Serial.print("Confirmation Message: ");
          Serial.println(received_value);
          successful_packet = true;
          /* The transmitted data are being processed by the receiver. The receiver then sends
          // control data to this node. These data are how much to open the valve of the balloon
          // and at what speed(frequency/period) to turn the stepper motors in order to produce 
          // the desired force to the balloon and move it.
          // To simplify this implementation we are not using a valve and only one stepper motor
          // is used, hence we receive one value which is the period of the stepper motor rotation
          // which sequentually controls the balloon.*/
          // initialize all required parameters for data receive
          uint8_t buf[RF22_ROUTER_MAX_MESSAGE_LEN];
          char incoming[RF22_ROUTER_MAX_MESSAGE_LEN];
          memset(buf, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
          memset(incoming, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
          uint8_t len = sizeof(buf);
          uint8_t from;
          //receive data, wait for a maximum of 5 seconds
          if(rf22.recvfromAckTimeout(buf, &len, 5000, &from)){
            buf[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
            memcpy(incoming, buf, RF22_ROUTER_MAX_MESSAGE_LEN);
            control_variable = atoi((char*)incoming);
            Serial.print("Control variable: ");
            Serial.println(control_variable);
          }
        }
        else{
          Serial.println("Wrong Transmitter Data");
          
        }
      }
      current_time = millis();
    }
    //[OUTDATED CODE] THIS IS BASED ON SENDING THE SENSOR MEASUREMENTS SEPARATE AND WITHOUT ANY LOOP
    /*memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    sprintf(data_read, "%d", sensorVal_1);
    data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
    memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);
    initial_time = millis();
    if (rf22.sendtoWait(data_send, sizeof(data_send), DESTINATION_ADDRESS) != RF22_ROUTER_ERROR_NONE)
    {
      Serial.println("sendtoWait failed");
      randNumber=random(200,max_delay);
      Serial.println(randNumber);
      delay(randNumber);
      failed_transmissions++;
    }
    else
    {
      successful_counter++;
      correct_transmissions++;
      numberOfBytes=sizeof(data_send);
      totalNumberOfBytes+=numberOfBytes;
      current_time = millis();
      delta_time+= current_time - initial_time;
      //Serial.println("sendtoWait Succesful");

      uint8_t buf[RF22_ROUTER_MAX_MESSAGE_LEN];
      char incoming[RF22_ROUTER_MAX_MESSAGE_LEN];
      memset(buf, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      memset(incoming, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      uint8_t len = sizeof(buf);
      uint8_t from;
      int received_value = 0;
      rf22.recvfromAckTimeout(buf, &len, 3000, &from);
      buf[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
      memcpy(incoming, buf, RF22_ROUTER_MAX_MESSAGE_LEN);
      received_value = atoi((char*)incoming);
      if (received_value == SOURCE_ADDRESS){
      Serial.print("Confirmation Message: ");
      Serial.println(received_value);
      }
      else{
        Serial.println("Wrong Transmitter Data");
      }
    }
    
    memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    sprintf(data_read, "%d", sensorVal_2);
    data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
    memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);
    initial_time = millis();
    if (rf22.sendtoWait(data_send, sizeof(data_send), DESTINATION_ADDRESS) != RF22_ROUTER_ERROR_NONE)
    {
      Serial.println("sendtoWait failed");
      randNumber=random(200,max_delay);
      Serial.println(randNumber);
      delay(randNumber);
      failed_transmissions++;
    }
    else
    {
      successful_counter++;
      correct_transmissions++;
      numberOfBytes=sizeof(data_send);
      totalNumberOfBytes+=numberOfBytes;
      current_time = millis();
      delta_time+= current_time - initial_time;
      //Serial.println("sendtoWait Succesful");

      uint8_t buf[RF22_ROUTER_MAX_MESSAGE_LEN];
      char incoming[RF22_ROUTER_MAX_MESSAGE_LEN];
      memset(buf, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      memset(incoming, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      uint8_t len = sizeof(buf);
      uint8_t from;
      int received_value = 0;
      rf22.recvfromAckTimeout(buf, &len, 3000, &from);
      buf[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
      memcpy(incoming, buf, RF22_ROUTER_MAX_MESSAGE_LEN);
      received_value = atoi((char*)incoming);
      if (received_value == SOURCE_ADDRESS){
      Serial.print("Confirmation Message: ");
      Serial.println(received_value);
      }
      else{
        Serial.println("Wrong Transmitter Data");
      }

    }
    
    memset(data_read, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    memset(data_send, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
    sprintf(data_read, "%d", sensorVal_3);
    data_read[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
    memcpy(data_send, data_read, RF22_ROUTER_MAX_MESSAGE_LEN);
    initial_time = millis();
    if (rf22.sendtoWait(data_send, sizeof(data_send), DESTINATION_ADDRESS) != RF22_ROUTER_ERROR_NONE)
    {
      Serial.println("sendtoWait failed");
      randNumber=random(200,max_delay);
      Serial.println(randNumber);
      delay(randNumber);
      failed_transmissions++;
    }
    else
    {
      successful_counter++;
      correct_transmissions++;
      numberOfBytes=sizeof(data_send);
      totalNumberOfBytes+=numberOfBytes;
      current_time = millis();
      delta_time+= current_time - initial_time;
      //Serial.println("sendtoWait Succesful");

      uint8_t buf[RF22_ROUTER_MAX_MESSAGE_LEN];
      char incoming[RF22_ROUTER_MAX_MESSAGE_LEN];
      memset(buf, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      memset(incoming, '\0', RF22_ROUTER_MAX_MESSAGE_LEN);
      uint8_t len = sizeof(buf);
      uint8_t from;
      int received_value = 0;
      rf22.recvfromAckTimeout(buf, &len, 3000, &from);
      buf[RF22_ROUTER_MAX_MESSAGE_LEN - 1] = '\0';
      memcpy(incoming, buf, RF22_ROUTER_MAX_MESSAGE_LEN);
      received_value = atoi((char*)incoming);
      if (received_value == SOURCE_ADDRESS){
      Serial.print("Confirmation Message: ");
      Serial.println(received_value);
      }
      else{
        Serial.println("Wrong Transmitter Data");
      }
      
    }
    
    if(successful_counter==3){
      successful_packet = true;
      Serial.println("sendtoWait Succesful");
    }*/
  }
//}
// when the current time is more than the diagnostics period, print and reset all diagnostics
if((current_time - start_time)>=measurement_time){
  successful_perc=(float)correct_transmissions/(correct_transmissions+failed_transmissions);
  correct_transmissions=0;
  failed_transmissions=0;
  Serial.println(); 
  Serial.println("=== Diagnostics ==="); 
  Serial.print("Successful Ratio= "); 
  Serial.println(successful_perc);

  throughput= 8*(float)(totalNumberOfBytes)*1000/delta_time;
  delta_time=0;
  totalNumberOfBytes=0;
  Serial.print("Throughput= "); 
  Serial.print(throughput); 
  Serial.println("bps");

  start_time=millis();
}
Serial.println(); 
}
